# read_database.py

import mysql.connector as mc

conn= mc.connect(host='localhost',user='root',password='d',db='pythonSQL')
c = conn.cursor()

def read_data():
    c.execute('SELECT * FROM writer')
    writers = c.fetchall()
    for writer in writers:
        print(writer)


def main():
    read_data()

if __name__ == '__main__':
    main()
